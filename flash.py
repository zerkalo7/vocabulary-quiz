import random
import os
import math
import re

vocabulary = {}
vocabulary_mistakes = {}
total_answers_counter = 0
correct_answers_counter = 0
vocabulary_file = 'vocabulary.lst'
number_of_answers = 50

def info_words():
    print('Total words translated: ' + str(total_answers_counter) + '   Correct translations: ' + str(correct_answers_counter)) 

def success_percentage():
    print('Success percentage: ' + str(math.floor((correct_answers_counter) / total_answers_counter * 100)) + '%')

def show_vocabulary_mistakes():
    print('You did mistakes in:')
    for k, v in vocabulary_mistakes.items():
        print(k, v)

def check_if_all_score_exists(file_path, extra_symbols):
    with open(file_path, 'r+') as file:
        lines = file.readlines()
        file.seek(0) 
        for line in lines:
            line = line.strip() 
            print(line)
            if re.match(r'.*[^0-9]\D$', line):
                line += extra_symbols 
            file.write(line + '\n')

def proccess_vocabulary(vocabulary_file, find_string, plus_or_minus):
    with open(vocabulary_file, 'r') as file:
        lines = file.readlines()

    with open(vocabulary_file, 'w') as file:
        for line in lines:
            processed_line = change_score(line, find_string, plus_or_minus)
            file.write(processed_line)

def change_score(line, find_string, plus_or_minus):
    if line.startswith(find_string):
        words, score = line.rsplit(" ", 1)
        if plus_or_minus == "plus":
            score_modified = int(score) + 10
        else:
            score_modified = int(score) - 5
        line = line.replace(score, str(score_modified))
        line = line + '\n'
        return line
    else:
        return line

#CODE

check_if_all_score_exists(vocabulary_file," 50")

while total_answers_counter < number_of_answers:

    with open(vocabulary_file) as myvocabulary:
        for line in myvocabulary:
            if re.match("#",line):
                pass
            else:
                words, rating = line.rsplit(" ",1)
                if int(rating) > 0:
                    vocabulary[words.strip()] = int(rating.strip())
                else:
                    pass


    randomized_pair = str(random.choices(list(vocabulary.keys()), weights=vocabulary.values(), k=1))
    english, russian = randomized_pair.split(" ", 1)

    os.system('clear')
    info_words()
    print('----------')
    print(russian[:-2])
    answer = input()
    total_answers_counter += 1

    if answer == english[2:]:
        correct_answers_counter += 1
        proccess_vocabulary(vocabulary_file, english[2:],'minus')       
    elif answer == 'quit':
        os.system('clear')
        info_words()
        success_percentage()
        quit()
    else:
        print('False. Right answer is ' + english[2:] + '.')
        proccess_vocabulary(vocabulary_file, english[2:], "plus")
        vocabulary_mistakes.update({ english[2:]: russian[:-2] })
        correct_answer = 0
        while correct_answer != english[2:]:
            correct_answer = input('Please type right answer to continue: ')
        
else:
    os.system('clear')
    print('Test finished. \n')
    info_words()
    success_percentage()
    print('')
    show_vocabulary_mistakes()
